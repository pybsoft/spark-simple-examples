package com.pybsoft;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.stream.Collectors;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.hbase.HBaseConfiguration;

import org.apache.hadoop.hbase.spark.JavaHBaseContext;
import org.apache.hadoop.hbase.spark.datasources.HBaseTableCatalog;

import org.apache.spark.SparkConf;

import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.sql.Dataset;

import org.apache.spark.sql.Row;
import org.apache.spark.sql.SQLContext;


/**
 * 
 * Connects to a Hbase cluster, running in local but deploing executors on hbase cluster
 * This class uses the hbase-spark connector from Hbase
 * @author jreina
 *
 */
public class MainClass {

	//Path with a copy of hbase-site.xml
	private static String HBASE_CONF = "/home/hadoop/hbase/conf";
	
	//Path with a copy of core-site.xml
	private static String HADOOP_CONF = "/home/hadoop/hadoop/etc/hadoop";
	
	//Path to schema file
	private static String SCHEMA_FILE = "/home/hadoop/example-schema.json";

	@SuppressWarnings("deprecation")
	public static void main(String[] args) {


		// Configure Spark
		SparkConf sparkConf = new SparkConf().setAppName("HBaseSparkLocalExample").setMaster("local[*]");

		// Create a Spark Context with an app name
		JavaSparkContext sparkContext = new JavaSparkContext(sparkConf);

		// Setting Level to Info
		sparkContext.setLogLevel("ERROR");

		// Create a configuration object
		Configuration hbaseConf = HBaseConfiguration.create();

		hbaseConf.addResource(new Path(HBASE_CONF, "hbase-site.xml"));
		hbaseConf.addResource(new Path(HADOOP_CONF, "core-site.xml"));

		JavaHBaseContext hbaseContext = new JavaHBaseContext(sparkContext, hbaseConf);
		
		SQLContext sqlContext = new SQLContext(hbaseContext.jsc());
		
		String tableSchema = null;

		try {
			
			//This contains a reduced schema of the real table. Since running on local mode, the path can be a path to a local file
			tableSchema = Files.lines(Paths.get(SCHEMA_FILE), StandardCharsets.UTF_8)
					.collect(Collectors.joining("\n"));
		} catch (IOException e) {
			System.out.println("Error reading schema file");
			System.exit(1);
		}

		
		System.out.println("Connecting: ");
		
		// Connect and load Hbase table defined in schema file
		Dataset<Row> messagesTable = sqlContext.read()
				.option(HBaseTableCatalog.tableCatalog(), tableSchema)
				.format("org.apache.hadoop.hbase.spark").load();
		

		//Show all contents (It is a small table, so no problem of running out of heap
		messagesTable.show();
		
		
		sparkContext.close();

	}
}
